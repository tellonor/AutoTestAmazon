package NyTest;


import static org.testng.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;

//The purpose of this test is to verify that after opening amazon.com, one can search for Nikon in the landing side, sort the results from highest to lowest price
//choose the second element in the list and assert that the details contains the text Nikon 4DS. NB: notice that the assignment stated to check for Nikon 3DX

public class NewTest {
	
	public WebDriver driver;
	
@Test
public void openWebSite() {
 //Get the url parameter from pom.xml and open browser
 driver.get(System.getProperty("url"));
 driver.manage().window().maximize();
 WebDriverWait wait = new WebDriverWait(driver, 10);
 
 //Wait for the search field to be present
 wait.until(ExpectedConditions.presenceOfElementLocated(By.id("twotabsearchtextbox")));
 WebElement searchbox = driver.findElement(By.id("twotabsearchtextbox"));
 
 //Search for Nikon
 searchbox.click();
 searchbox.sendKeys("Nikon");
 WebElement magnifier = driver.findElement(By.className("nav-search-submit"));
 magnifier.click();
 
 //Sort the results from highest to lowest price
 Select sortdropdown = new Select(driver.findElement(By.id("sort")));
 sortdropdown.selectByVisibleText("Price: High to Low");
 System.out.println("Wait for sorted list to display");
 
 //Wait for the new sorted list and click on the second element
 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[@id='result_1']/div/div/div/div")));
 WebElement secondresult = driver.findElement(By.xpath("//li[@id='result_1']/div/div/div/div"));
 secondresult.click();
 System.out.println("Wait for details to display");

 //Wait until the details are displayed
 wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("productTitle")));
 String product = driver.findElement(By.id("productTitle")).getText();

 //Check that the description (in this case the title of it contains the text "Nikon D4S". One can change this to the preferred text
 assertTrue(product.contains("Nikon D4S"));
 System.out.println("Test Finished");
 
 }



@BeforeClass
public void beforeClass() {
//Choose the preferred webdriver and initialize. NB: This requires that the webdriver is located as specified under "C:\Selenium\Drivers
	System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\Drivers\\chromedriver.exe");
	driver = new ChromeDriver();
	}

@AfterClass
public void afterClass() {
	driver.quit();

}

}